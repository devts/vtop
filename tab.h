typedef struct tab tab;
struct tab
{
    size_t mrow;
    size_t mcol;
    procview *dv;
};
enum
{
    COL_PID,
    COL_UID,
    COL_PPID,
    COL_TTY_NAME,
    COL_VSIZE,
    COL_RSS,
    COL_STATE,
    COL_NICE,
    COL_NUM_THREADS,
    COL_STIME,
    COL_SUTIME_FINE,
    COL_CPU_PERCENT,
};

tab *tab_new(void);
int tab_del(tab *t);
int tab_print(tab *t, WINDOW *w);
void tab_bring_cur_in_view(tab *t);
int tab_move_to(tab *t, size_t n);
int tab_move_rel(tab *t, int n);
void tab_move_top(tab *t);
void tab_move_middle(tab *t);
void tab_move_bottom(tab *t);
void tab_page_up(tab *t, size_t n, size_t m, size_t sl);
void tab_page_down(tab *t, size_t n, size_t m, size_t sl);
void tab_cur_to_middle(tab *t);
void tab_cur_to_top(tab *t);
void tab_cur_to_bottom(tab *t);
int tab_search_next(tab *t, char *reg);
int tab_search_prev(tab *t, char *reg);
