#include "all.h"

/*tab prefs*/
size_t so = 8;
int tree = 1;
int sort = COL_PID;
int _cols[] = { COL_UID, COL_PID, COL_PPID, COL_TTY_NAME, COL_NUM_THREADS, COL_NICE, COL_VSIZE, COL_RSS,
    COL_STATE, COL_STIME, COL_SUTIME_FINE, COL_CPU_PERCENT };
int *cols = _cols;
size_t s_cols = LEN(_cols);

/*key from char*/
#define KFC(x) {.utfk = 1, .ku = { .r = (Rune)(x) }}
/*key from int*/
#define KFI(x) {.utfk = 0, .ku = { .k = (int)(x) }}
#define CTRL(a) ((a) - 'a' + 1)

/*keyconfig*/
/* normal mode keybindings */
keybind normal_kbs_[] = {
    /* key                                  s_keys          s_post      function                     argument                            */
    { (key[]){ KFC('j')},                   1,              0,          cmd_jk,                      {.i = 1  },                         },
    { (key[]){ KFC('k')},                   1,              0,          cmd_jk,                      {.i = -1 },                         },
    { (key[]){ KFC('G')},                   1,              0,          cmd_G,                       {.i = 0  },                         },
    { (key[]){ KFC('/')},                   1,              0,          cmd_search,                  {.i = 0  },                         },
    { (key[]){ KFC('n')},                   1,              0,          cmd_search_next,             {.i = 0  },                         },
    { (key[]){ KFC('N')},                   1,              0,          cmd_search_prev,             {.i = 0  },                         },
    { (key[]){ KFC(':')},                   1,              0,          cmd_entercmd,                {.i = 0  },                         },
    { (key[]){ KFC(' ')},                   1,              0,          cmd_togglesel,               {.i = 0  },                         },
    { (key[]){ KFC('t')},                   1,              0,          cmd_toggle_tree,             {.i = 0  },                         },
    { (key[]){ KFC('r')},                   1,              0,          cmd_force_update,            {.i = 0  },                         },
    { (key[]){ KFC('l')},                   1,              0,          cmd_lsof,                    {.i = 0  },                         },
    { (key[]){ KFC('q')},                   1,              0,          cmd_exit,                    {.i = 0  },                         },
    { (key[]){ KFC(CTRL('b'))},             1,              0,          cmd_page_up,                 {.i = 0  },                         },
    { (key[]){ KFC(CTRL('f'))},             1,              0,          cmd_page_down,               {.i = 0  },                         },
    { (key[]){ KFC(CTRL('u'))},             1,              0,          cmd_scroll_up,               {.i = 0  },                         },
    { (key[]){ KFC(CTRL('d'))},             1,              0,          cmd_scroll_down,             {.i = 0  },                         },
    { (key[]){ KFI(KEY_DOWN)},              1,              0,          cmd_jk,                      {.i = 1  },                         },
    { (key[]){ KFI(KEY_UP)},                1,              0,          cmd_jk,                      {.i = -1 },                         },
    { (key[]){ KFC('d'), KFC('d')},         2,              0,          cmd_kill,                    {.i = SIGTERM},                     },
    { (key[]){ KFC('D'), KFC('D')},         2,              0,          cmd_kill,                    {.i = SIGKILL},                     },
    { (key[]){ KFC('z'), KFC('Z')},         2,              0,          cmd_kill,                    {.i = SIGTSTP},                     },
    { (key[]){ KFC('Z'), KFC('Z')},         2,              0,          cmd_kill,                    {.i = SIGSTOP},                     },
    { (key[]){ KFC('z'), KFC('!')},         2,              0,          cmd_kill,                    {.i = SIGCONT},                     },
    { (key[]){ KFC('g'), KFC('g')},         2,              0,          cmd_gg,                      {.i = 0  },                         },
    { (key[]){ KFC('z'), KFC('h')},         2,              0,          cmd_move_top,                {.i = 0  },                         },
    { (key[]){ KFC('z'), KFC('m')},         2,              0,          cmd_move_middle,             {.i = 0  },                         },
    { (key[]){ KFC('z'), KFC('l')},         2,              0,          cmd_move_bottom,             {.i = 0  },                         },
    { (key[]){ KFC('z'), KFC('z')},         2,              0,          cmd_cur_to_middle,           {.i = 0  },                         },
    { (key[]){ KFC('z'), KFC('t')},         2,              0,          cmd_cur_to_top,              {.i = 0  },                         },
    { (key[]){ KFC('z'), KFC('b')},         2,              0,          cmd_cur_to_bottom,           {.i = 0  },                         },
    { (key[]){ KFC('o'), KFC('p')},         2,              0,          cmd_order,                   {.i = COL_PID},                     },
    { (key[]){ KFC('o'), KFC('m')},         2,              0,          cmd_order,                   {.i = COL_RSS},                     },
    { (key[]){ KFC('o'), KFC('t')},         2,              0,          cmd_order,                   {.i = COL_SUTIME_FINE},             },
    { (key[]){ KFC('o'), KFC('c')},         2,              0,          cmd_order,                   {.i = COL_CPU_PERCENT},             },
};
keybind *normal_kbs = normal_kbs_;
size_t s_normal_kbs = LEN(normal_kbs_);
keybind *normal_fallthr = 0;

keybind kb_prompt_ins =
    { (key[]){ KFC('j')},                   1,              0,          cmd_prompt_ins,                      {.i =1  },                  };

/* cmd mode keybindings */
keybind cmd_kbs_[] = {
	/* key                                  s_keys          s_post      function                     argument                            */
    { (key[]){ KFC(27)},                    1,              0,          cmd_prompt_exit,             {.i =1  },                          },
    { (key[]){ KFI(KEY_LEFT)},              1,              0,          cmd_prompt_curs_left,        {.i =1  },                          },
    { (key[]){ KFI(KEY_RIGHT)},             1,              0,          cmd_prompt_curs_right,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('a'))},             1,              0,          cmd_prompt_curs_start,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('e'))},             1,              0,          cmd_prompt_curs_end,         {.i =1  },                          },
    { (key[]){ KFI(KEY_BACKSPACE)},         1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFC(127)},                   1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFI(KEY_DC)},                1,              0,          cmd_prompt_curs_dc,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('u'))},             1,              0,          cmd_prompt_curs_dl,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('k'))},             1,              2,          cmd_prompt_ins_digraph,      {.i =1  },                          },
    { (key[]){ KFC('\t')},                  1,              0,          cmd_prompt_complete,         {.i =1  },                          },
    { (key[]){ KFC('\n')},                  1,              0,          cmd_prompt_exec,             {.i =1  },                          },
    { (key[]){ KFI(KEY_UP)},                1,              0,          cmd_switch_hist,             {.i =-1 },                          },
    { (key[]){ KFI(KEY_DOWN)},              1,              0,          cmd_switch_hist,             {.i =1  },                          },
};
keybind *cmd_kbs = cmd_kbs_;
size_t s_cmd_kbs = LEN(cmd_kbs_);
keybind *cmd_fallthr = &kb_prompt_ins;

/* search mode keybindings */
keybind search_kbs_[] = {
	/* key                                  s_keys          s_post      function                     argument                            */
    { (key[]){ KFC(27)},                    1,              0,          cmd_prompt_exit,             {.i =1  },                          },
    { (key[]){ KFI(KEY_LEFT)},              1,              0,          cmd_prompt_curs_left,        {.i =1  },                          },
    { (key[]){ KFI(KEY_RIGHT)},             1,              0,          cmd_prompt_curs_right,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('a'))},             1,              0,          cmd_prompt_curs_start,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('e'))},             1,              0,          cmd_prompt_curs_end,         {.i =1  },                          },
    { (key[]){ KFI(KEY_BACKSPACE)},         1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFC(127)},                   1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFI(KEY_DC)},                1,              0,          cmd_prompt_curs_dc,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('u'))},             1,              0,          cmd_prompt_curs_dl,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('k'))},             1,              2,          cmd_prompt_ins_digraph,      {.i =1  },                          },
    { (key[]){ KFC('\t')},                  1,              0,          cmd_prompt_complete,         {.i =1  },                          },
    { (key[]){ KFC('\n')},                  1,              0,          cmd_prompt_search_submit,    {.i =1  },                          },
    { (key[]){ KFI(KEY_UP)},                1,              0,          cmd_switch_hist,             {.i =-1 },                          },
    { (key[]){ KFI(KEY_DOWN)},              1,              0,          cmd_switch_hist,             {.i =1  },                          },
};
keybind *search_kbs = search_kbs_;
size_t s_search_kbs = LEN(search_kbs_);
keybind *search_fallthr = &kb_prompt_ins;

/*cmdline prefs*/
Cmd _cmds[] = {
	/* name         s_name      func    */
	{ "s",          1,          cmd_regsel      },
	{ "cs",         2,          cmd_clearsel    },
};
Cmd *cmds = _cmds;
size_t s_cmds = LEN(_cmds);

/*colorscheme*/
int initcolorscheme(void)
{
   scheme.DEFAULT_ATTR = getcolor(COLOR_WHITE, COLOR_BLACK);
   scheme.ERROR_ATTR = getcolor(COLOR_RED, COLOR_BLACK) | A_BOLD;
   scheme.HEADER_ATTR = getcolor(COLOR_BLACK, COLOR_GREEN);
   scheme.PROC_SELF_ATTR = getcolor(COLOR_WHITE, COLOR_BLACK);
   scheme.PROC_OTHR_ATTR = getcolor(COLOR_CYAN, COLOR_BLACK);
   scheme.CUR_OR_ATTR = A_REVERSE | A_BOLD;
   scheme.SELECTED_ATTR = getcolor(COLOR_YELLOW, COLOR_BLACK) | A_BOLD;
   return 0;
}
