#include "../all.h"
#include "digraph_gen.h"

int
digraph2cmp(const void *v1, const void *v2) {
    const Digraph *dg1 = v1;
    const Digraph *dg2 = v2;
    if (dg1->a != dg2->a)
        return (int)dg1->a - (int)dg2->a;
    return (int)dg1->b - (int)dg2->b;
}

Rune
digraphtorune(char a, char b) {
    Digraph dg = {a, b, 0};
    Digraph *match = 0;
    if ((match = bsearch(&dg, digraphs, s_digraphs, sizeof(Digraph), &digraph2cmp)))
        return match->r;
    return Runeerror;
}
