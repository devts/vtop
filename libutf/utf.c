#include "../all.h"

int
badrune(Rune x) {
    return ((x) < 0 || (x) > Runemax \
        || ((x) & 0xFFFE) == 0xFFFE \
        || ((x) >= 0xD800 && (x) <= 0xDFFF) \
        || ((x) >= 0xFDD0 && (x) <= 0xFDEF));
}

int
runelen(Rune r) {
    if(badrune(r))
        return 0; /* error */
    else if(r <= 0x7F)
        return 1;
    else if(r <= 0x07FF)
        return 2;
    else if(r <= 0xFFFF)
        return 3;
    else
        return 4;
}

size_t
runenlen(const Rune *p, size_t len) {
    size_t n = 0;
    for(size_t i = 0; i < len; i++)
        n += runelen(p[i]);
    return n;
}

unsigned int
utfseq(char x) {
    return ((((x) & 0x80) == 0x00) ? 1 /* 0xxxxxxx */ \
          : (((x) & 0xC0) == 0x80) ? 0 /* 10xxxxxx */ \
          : (((x) & 0xE0) == 0xC0) ? 2 /* 110xxxxx */ \
          : (((x) & 0xF0) == 0xE0) ? 3 /* 1110xxxx */ \
          : (((x) & 0xF8) == 0xF0) ? 4 /* 11110xxx */ \
          : (((x) & 0xFC) == 0xF8) ? 5 /* 111110xx */ \
          : (((x) & 0xFE) == 0xFC) ? 6 /* 1111110x */ \
                                   : 0 );
}

/* Try to decode a single rune starting at s.
 * This function will not read past a null byte, so passing C strings is fine.
 * Returns the number of bytes read for the current rune or 0 if rune is incomplete */
int
charntorune(Rune *p, const char *s, size_t len) {
    if(!len)
        return *p = Runeerror, 0;

  unsigned int n = utfseq(s[0]);
  if (!n)
        return *p = Runeerror, 1;
  uint32_t mask[6] = {0xFF, 0x1F, 0x0F, 0x07, 0x03, 0x01};
  Rune r = s[0] & mask[n - 1];

    /* add values from continuation bytes */
    unsigned int i;
    for (i = 1; i < MIN(n, len); i++) {
        if ((s[i] & 0xC0) == 0x80)
            r = (r << 6) | (s[i] & 0x3F); /* 10xxxxxx */
        else
            return *p = Runeerror, i;
    }

    if (i < n) /* must have reached len limit */
        return *p = Runeerror, 0;

    /* reject invalid or overlong sequences */
    if (badrune(r) || runelen(r) < (int)n)
        r = Runeerror;

    *p = r;
    return n;
}

int
chartorune(Rune *p, const char *s) {
    return charntorune(p, s, UTFmax);
}

int
runetocharn(char *s, size_t len, const Rune *p) {
    Rune r = *p;

    unsigned int rl = runelen(r);
    if (len < rl)
        return 0;
    switch(rl) {
    case 1: /* 0aaaaaaa */
        s[0] = r;
        return 1;
    case 2: /* 00000aaa aabbbbbb */
        s[0] = 0xC0 | ((r & 0x0007C0) >>  6); /* 110aaaaa */
        s[1] = 0x80 |  (r & 0x00003F);        /* 10bbbbbb */
        return 2;
    case 3: /* aaaabbbb bbcccccc */
        s[0] = 0xE0 | ((r & 0x00F000) >> 12); /* 1110aaaa */
        s[1] = 0x80 | ((r & 0x000FC0) >>  6); /* 10bbbbbb */
        s[2] = 0x80 |  (r & 0x00003F);        /* 10cccccc */
        return 3;
    case 4: /* 000aaabb bbbbcccc ccdddddd */
        s[0] = 0xF0 | ((r & 0x1C0000) >> 18); /* 11110aaa */
        s[1] = 0x80 | ((r & 0x03F000) >> 12); /* 10bbbbbb */
        s[2] = 0x80 | ((r & 0x000FC0) >>  6); /* 10cccccc */
        s[3] = 0x80 |  (r & 0x00003F);        /* 10dddddd */
        return 4;
    default:
        return 0; /* error */
    }
}

int
runetochar(char *s, const Rune *p) {
    return runetocharn(s, 4, p);
}

int
runetocharm(char **s, size_t *len, Rune *p) {
    unsigned int rl = runelen(*p);
    if (!rl)
        return *s = 0, 0;
    *s = malloc(rl);
    *len = rl;
    return runetocharn(*s, rl, p);
}

int
utftorunestrm(const char *c, Rune **ch, size_t *len) {
    size_t msize = 0;
    *len = 0;
    *ch = 0;
    Rune uch;
    int d;
    int err = 0;
    while (c && (d = chartorune(&uch, c) > 0)) {
        if (uch == Runeerror)
            err = 1;
        list_append((void **)ch, len, &msize, sizeof(Rune), &uch);
        c += d;
    }
    list_malloc_trim((void **)ch, len, &msize, sizeof(Rune));
    return err;
}

int
utfnext(const char *s, ssize_t len, size_t base_in, size_t *base_out) {
    if (!s[base_in])
        return *base_out = base_in, -1;
    size_t cur = base_in;
    while ((len < 0 ? 1 : cur + 1 < len) && s[++cur] && (s[cur] & 0xC0) == 0x80)
        ;
    if ((s[cur] & 0xC0) == 0x80 || cur == base_in)
        return *base_out = base_in, -1;
    return *base_out = cur, 0;
    return 0;
}

int
utfprev(const char *s, ssize_t len, size_t base_in, size_t *base_out) {
    size_t cur = base_in;
    while (cur && (s[--cur] & 0xC0) == 0x80)
        ;
    if ((s[cur] & 0xC0) == 0x80 || cur == base_in)
        return *base_out = base_in, -1;
    return *base_out = cur, 0;
}

int
utfseek(const char *s, ssize_t len, size_t base_in, size_t *base_out, long off) {
    if (!off)
        return *base_out = base_in, 0;
    int prev = off < 0;
    off = prev ? -off : off;
    for (; off; off--) {
        if((prev ? utfprev : utfnext)(s, len, base_in, &base_in))
            return *base_out = base_in, -1;
    }
    return *base_out = base_in, 0;
}

size_t
wcwidth_range(char *c, ssize_t len, size_t base_start, size_t base_end) {
    if (base_start >= len || base_end >= len)
        return 0;
    Rune r;
    size_t wcw = 0;
    size_t d = 0;
    while (base_start <= base_end
            && (d = charntorune(&r, c + base_start, len < 0 ? UTFmax : len - base_start)) > 0) {
        wcw += MAX(wcwidth(r), 0);
        base_start += d;
  }
  return wcw;
}

size_t
utfbasetoend(char *s, ssize_t len, size_t base_in) {
    if (!s[base_in])
        return base_in;
    size_t cur = base_in;
    while ((len < 0 ? 1 : cur + 1 < len) && s[++cur] && (s[cur] & 0xC0) == 0x80)
        ;
    if ((s[cur] & 0xC0) == 0x80 || cur == base_in)
        return cur;
    return cur - 1;
}


/* seek forward (max >= 0) or backwards (max < 0)
 * such that the sum of wcwidth of chars between base_in and *base_out
 * is maximal while still beeing smaller or equal to max */
int
wcwidth_seek_limited(char *c, ssize_t len, size_t base_in, size_t *base_out, size_t *wcw_range, long max) {
    if (!len || (len > 0 && base_in >= len))
        return *base_out = base_in, -1;
    int prev = max < 0;
    max = prev ? -max : max;
    Rune r;
    size_t wcw = 0;
    ssize_t cur = -1;
    size_t next = base_in;
    while (charntorune(&r, c + next, len < 0 ? UTFmax : len - next) > 0) {
        size_t wcc = MAX(wcwidth(r), 0);
        if (wcw + wcc > max)
            break;
        wcw += wcc;
        cur = next;
        if ((prev ? utfprev : utfnext)(c, len, cur, &next))
            break;
    }
    if (cur < 0) /*request is insatisfiable, i.e. wcwidth(c[base_in]) > max */
        return *base_out = base_in, -1;
    return *base_out = cur, *wcw_range = wcw, 0;
}

int
fgetrune(Rune *r, FILE *fp) {
    char buf[UTFmax];
    int  i = 0, c;

    while (i < UTFmax && (c = fgetc(fp)) != EOF) {
        buf[i++] = c;
        if (charntorune(r, buf, i) > 0)
            break;
    }
    if (ferror(fp))
        return -1;

    return i;
}

int
efgetrune(Rune *r, FILE *fp, const char *file) {
    int ret;

    if ((ret = fgetrune(r, fp)) < 0) {
        fprintf(stderr, "fgetrune %s: %s\n", file, strerror(errno));
        exit(1);
    }
    return ret;
}

int
fputrune(const Rune *r, FILE *fp) {
    char buf[UTFmax];

    return fwrite(buf, runetochar(buf, r), 1, fp);
}

int
efputrune(const Rune *r, FILE *fp, const char *file) {
    int ret;

    if ((ret = fputrune(r, fp)) < 0) {
        fprintf(stderr, "fputrune %s: %s\n", file, strerror(errno));
        exit(1);
    }
    return ret;
}

size_t
utflen(const char *s) {
    const char *p = s;
    size_t i;
    Rune r;

    for(i = 0; *p != '\0'; i++)
        p += chartorune(&r, p);
    return i;
}

size_t
utfnlen(const char *s, size_t len) {
    const char *p = s;
    size_t i;
    Rune r;
    int n;

    for(i = 0; (n = charntorune(&r, p, len-(p-s))) && r != '\0'; i++)
        p += n;
    return i;
}

char *
utfrune(const char *s, Rune r) {
    if(r < Runeself) {
        return strchr(s, r);
    }
    else if(r == Runeerror) {
        Rune r0;
        int n;

        for(; *s != '\0'; s += n) {
            n = chartorune(&r0, s);
            if(r == r0)
                return (char *)s;
        }
    }
    else {
        char buf[UTFmax+1];
        int n;

        if(!(n = runetochar(buf, &r)))
            return NULL;
        buf[n] = '\0';
        return strstr(s, buf);
    }
    return NULL;
}

char *
utfrrune(const char *s, Rune r) {
    const char *p = NULL;
    Rune r0;
    int n;

    if(r < Runeself)
        return strrchr(s, r);

    for(; *s != '\0'; s += n) {
        n = chartorune(&r0, s);
        if(r == r0)
            p = s;
    }
    return (char *)p;
}

char *
utfutf(const char *s, const char *t) {
    const char *p, *q;
    Rune r0, r1, r2;
    int n, m;

    for(chartorune(&r0, t); (s = utfrune(s, r0)); s++) {
        for(p = s, q = t; *q && *p; p += n, q += m) {
            n = chartorune(&r1, p);
            m = chartorune(&r2, q);
            if(r1 != r2)
                break;
        }
        if(!*q)
            return (char *)s;
    }
    return NULL;
}
