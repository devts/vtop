/*tab prefs*/
size_t so;
int tree;
int sort;

int *cols;
size_t s_cols;

/*keyconfig*/
keybind *normal_kbs;
size_t s_normal_kbs;
keybind *normal_fallthr;

keybind *cmd_kbs;
size_t s_cmd_kbs;
keybind *cmd_fallthr;

keybind *search_kbs;
size_t s_search_kbs;
keybind *search_fallthr;

/*cmdline prefs*/
Cmd *cmds;
size_t s_cmds;

/*colorscheme*/
int initcolorscheme(void);
