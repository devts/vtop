#include "../all.h"

enum { C_NOTSET, C_LOWER, C_ONELOWER, C_ONEUPPER, C_UPPER };
static void
lstrncatmc(char **dst, size_t *dst_num, size_t *dst_msize, char *src, size_t n, int *conv)
{
    if (!n)
        return;
    size_t slen = strnlen(src, n);
    size_t i = 0;
    Rune r;
    int d, e;
    char buf[UTFmax];
    if (*conv != C_NOTSET) {
        while (i < slen) {
            d = charntorune(&r, src + i, slen - i);
            if (!d || r == Runeerror)
                break; /*error decodeing utf8, do garbage in garbage out for remainder of src*/
            r = (*conv == C_LOWER || *conv == C_ONELOWER) ? tolowerrune(r) : toupperrune(r);
            e = runetocharn(buf, sizeof(buf), &r);
            if (!e)
                break;
            list_append_range((void **)dst, dst_num, dst_msize, 1, e, buf);
            i += d;
            if (*conv == C_ONELOWER || *conv == C_ONEUPPER) {
                *conv = C_NOTSET;
                break;
            }
        }
    }
    list_append_range((void **)dst, dst_num, dst_msize, 1, slen - i, src + i);
}
char *
subst(char *cmd, char *str)
{
    char *res = 0;
    size_t res_num = 0;
    size_t res_msize = 0;
    char *s, *t, *p;
    char *regex = emalloc(strlen(cmd));
    char *subst = emalloc(strlen(cmd));
    char *opts = emalloc(strlen(cmd));
    int gflag = 0;
    int cflags = REG_EXTENDED;
    int eflags = 0;
    regex_t re;
    regmatch_t *rm, pmatch[10];
    size_t len;
    int matches = 0, last_empty = 1, qflag = 0;
    int conv = C_NOTSET;

    if(!cmd || !str)
        goto subst_error;

    t = cmd;
    if(*t++ != 's' || *t++ != '/')
        goto subst_error;
    for(size_t i = 0; i < 3; i++) {
        if (i == 0)
            s = regex;
        else if (i == 1)
            s = subst;
        else if (i == 2)
            s = opts;
        while (*t) {
            if (*t=='/') {
                t++;
                break;
            }
            if (*t == '\\') {
                if (t[1] == '/')
                    ++t;
                else if (t[1] == '\\')
                    *s++ = *t++;
            }
            *s++ = *t++;
        }
        *s = 0;
    }
    if (*t)
        goto subst_error;

    s = opts;
    while (*s) {
        switch (*s) {
        case 'g':
            gflag = 1;
            break;
        case 'i':
        case 'I':
            cflags |= REG_ICASE;
            break;
        default:
            goto subst_error;
        }
        s++;
    }

    if (regcomp(&re, regex, cflags))
        goto subst_error;

    s = str;
    list_minsize((void **)&res, &res_num, &res_msize, 1, MAX(strlen(str), 4096));
    *res = 0;

    while (!qflag && !regexec(&re, s, 10, pmatch, eflags)) {
        eflags |= REG_NOTBOL; /* match against beginning of line first time, but not again */
        if (!*s) /* match against empty string first time, but not again */
            qflag = 1;

        /* don't substitute if last match was not empty but this one is.
         * s_a*_._g
         * foobar -> .f.o.o.b.r.
         */
        if ((last_empty || pmatch[0].rm_eo) && (++matches == (gflag ? 0 : 1) || gflag)) {
            /* copy over everything before the match */
            lstrncatmc(&res, &res_num, &res_msize, s, pmatch[0].rm_so, &conv);
            /* copy over replacement text, taking into account &, backreferences, and \ escapes */
            for (p = subst, len = strcspn(p, "\\&"); *p; len = strcspn(++p, "\\&")) {
                lstrncatmc(&res, &res_num, &res_msize, p, len, &conv);
                p += len;
                switch (*p) {
                default:
                    goto subst_error_reg;
                case '\0':
                    /* we're at the end, back up one so the ++p will put us on
                     * the null byte to break out of the loop */
                    --p;
                    break;
                case '&':
                    lstrncatmc(&res, &res_num, &res_msize, s + pmatch[0].rm_so, pmatch[0].rm_eo - pmatch[0].rm_so, &conv);
                    break;
                case '\\':
                    if (isdigit(*++p)) { /* backreference */
                        if ((size_t)(*p - '0') > 9)
                            goto subst_error_reg;
                        rm = &pmatch[*p - '0'];
                        lstrncatmc(&res, &res_num, &res_msize, s + rm->rm_so, rm->rm_eo - rm->rm_so, &conv);
                    } else {
                        switch (*p) {
                        default:
                            break;
                        case '\\':
                        case '&':
                            lstrncatmc(&res, &res_num, &res_msize, p, 1, &conv);
                            break;
                        case 'e':
                        case 'E':
                            conv = C_NOTSET;
                            break;
                        case 'l':
                            conv = C_ONELOWER;
                        case 'L':
                            conv = C_LOWER;
                            break;
                        case 'u':
                            conv = C_ONEUPPER;
                            break;
                        case 'U':
                            conv = C_UPPER;
                            break;
                        }
                    }
                    break;
                }
            }
            conv = C_NOTSET;
        } else {
            /* not replacing, copy over everything up to and including the match */
            lstrncatmc(&res, &res_num, &res_msize, s, pmatch[0].rm_eo, &conv);
        }

        if (!pmatch[0].rm_eo && *s) { /* empty match, advance one rune and add it to output */
            len = 1;
            while((s[len] & 0xc0) == 0x80)
                len++;
            lstrncatmc(&res, &res_num, &res_msize, s, len, &conv);
            s += len;
        }
        last_empty = !pmatch[0].rm_eo;
        s += pmatch[0].rm_eo;
    }
    lstrncatmc(&res, &res_num, &res_msize, s, strlen(s), &conv);

    char z = 0;
    if (res)
        list_append((void **)&res, &res_num, &res_msize, 1, &z);
    if (0) {
subst_error_reg:
        if (res)
            free(res);
        res = 0;
    }
    regfree(&re);
subst_error:
    free(regex);
    free(subst);
    free(opts);
    return res;
}
