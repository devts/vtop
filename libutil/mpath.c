#include "../all.h"

char *
pathmcat(const char *s, const char *t)
{
    size_t ss = strlen(s);
    size_t st = strlen(t);
    char *p;
    if (s[ss - 1] == '/') {
        p = emalloc(ss + st + 1);
        memcpy(p, s, ss);
        memcpy(p + ss, t, st);
        p[ss + st] = 0;
    } else {
        p = emalloc(ss + st + 2);
        memcpy(p, s, ss);
        p[ss] = '/';
        memcpy(p + ss + 1, t, st);
        p[ss + 1 + st] = 0;
    }
    return p;
}

char *
mdirname(const char *path) {
    char *pathc = estrdup(path);
    char *dn = dirname(pathc);
    char *r = estrdup(dn);
    free(pathc);
    return r;
}

char *
mbasename(const char *path) {
    char *pathc = estrdup(path);
    char *bn = basename(pathc);
    char *r = estrdup(bn);
    free(pathc);
    return r;
}

char *
mgetcwd(void)
{
    size_t msize = PATH_MAX;
    char *buf = emalloc(msize);
    while (!getcwd(buf, msize)) {
        if (errno == ENOMEM) {
            sleep(1u);
        } else if (errno == ERANGE) {
            free(buf);
            buf = emalloc(msize = msize << 1);
        } else {
            return 0;
        }
    }
    return buf;
}
