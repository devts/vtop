#include "all.h"

/*Normal mode cmds*/
void
cmd_force_update(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    dv_update(t->dv);
    print_all();
}
void 
cmd_gg(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_move_to(t, (pre ? pre - 1 : 0 ));
    print_all();
}
void 
cmd_G(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_move_to(t, (pre ? pre - 1 : t->dv->pl_size - 1));
    print_all();
}
void 
cmd_jk(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_move_rel(t, arg->i * (pre ? pre : 1 ));
    print_all();
}
void
cmd_move_top(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_move_top(t);
    print_all();
}
void
cmd_move_middle(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_move_middle(t);
    print_all();
}
void
cmd_move_bottom(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_move_bottom(t);
    print_all();
}
void
cmd_page_up(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_page_up(t, pre ? pre : 1, 1, 0);
    print_all();
}
void
cmd_page_down(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_page_down(t, pre ? pre : 1, 1, 0);
    print_all();
}
static size_t sl = 0;
void
cmd_scroll_up(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    if (pre)
        sl = pre;
    if (sl)
        tab_page_up(t, 0, 0, sl);
    else
        tab_page_up(t, 1, 2, 0);
    print_all();
}
void
cmd_scroll_down(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    if (pre)
        sl = pre;
    if (sl)
        tab_page_down(t, 0, 0, sl);
    else
        tab_page_down(t, 1, 2, 0);
    print_all();
}
void
cmd_cur_to_middle(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_cur_to_middle(t);
    print_all();
}
void
cmd_cur_to_top(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_cur_to_top(t);
    print_all();
}
void
cmd_cur_to_bottom(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_cur_to_bottom(t);
    print_all();
}

void
cmd_search(unsigned int pre, key *post, const Arg *arg, void *v)
{
    vcursctx_mode_switch(ctx, mode_search);
}

void
cmd_search_next(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_search_next(t, 0);
    print_all();
}
void
cmd_search_prev(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tab_search_prev(t, 0);
    print_all();
}
void
cmd_togglesel(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    dv_toggleselect_item(t->dv, t->dv->cur.n);
    tab_move_rel(t, 1);
    print_all();
}
void
cmd_exit(unsigned int pre, key *post, const Arg *arg, void *v)
{
    cleanup_exit(0);
}
static int
f_kill(pid_t pid, void *v)
{
    return kill(pid, *(int *)v);
}
void
cmd_kill(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    dv_iterate_as(t->dv, f_kill, (void *)&(arg->i));
    print_all();
}
void
cmd_order(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    sort = arg->i;
    dv_sort(t->dv);
    print_all();
}
void
cmd_toggle_tree(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    tree = !tree;
    dv_sort(t->dv);
    print_all();
}
void
cmd_lsof(unsigned int pre, key *post, const Arg *arg, void *v)
{
    tab *t = v;
    char buf[100];
    snprintf(buf, sizeof(buf), "%i", (int)t->dv->cur.pid);
    execfw("lsof", (char *[]){"lsof", "-p", buf, 0}, 0, 1, 1, 0, 0);
    print_all();
}
void
cmd_entercmd(unsigned int pre, key *post, const Arg *arg, void *v)
{
    vcursctx_mode_switch(ctx, mode_cmd);
}

/* Search / Cmd mode cmds*/
static void
mode_append_hist(mode *m, char *he)
{
    list_append((void **)&m->prompt_hist, &m->s_prompt_hist, &m->m_prompt_hist, sizeof(char *), &he);
    m->idx_hist_cur = m->s_prompt_hist;
}
static int
cmdmode_exec_prompt(void *v)
{
    if (!cmd_prompt->buf)
        return 0;
    int r = exec_cmd(cmd_prompt->buf, cmds, s_cmds, v);
    mode_append_hist(mode_cmd, strmcat("#", cmd_prompt->buf));
    prompt_freebuf(cmd_prompt);
    return r;
}
void
cmd_prompt_exec(unsigned int pre, key *post, const Arg *arg, void *v)
{
    cmdmode_exec_prompt(v);
    vcursctx_mode_switch(ctx, mode_normal);
}
void
cmd_switch_hist(unsigned int pre, key *post, const Arg *arg, void *v)
{
    mode *m = ctx->cur;
    if (!m || !(m == mode_cmd || m == mode_search))
        return;
    if (arg->i < 0)
        m->idx_hist_cur -= MIN(m->idx_hist_cur, -arg->i);
    else
        m->idx_hist_cur += MIN(m->s_prompt_hist - m->idx_hist_cur,  arg->i);
    char *he = 0;
    if (m->idx_hist_cur < m->s_prompt_hist)
        he = m->prompt_hist[m->idx_hist_cur];
    prompt_setstr(cmd_prompt, he);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_search_submit(unsigned int pre, key *post, const Arg *arg, void *v)
{
    if (cmd_prompt->buf)
        mode_append_hist(mode_search, estrdup(cmd_prompt->buf));
    prompt_freebuf(cmd_prompt);
    vcursctx_mode_switch(ctx, mode_normal);
}
void
cmd_prompt_exit(unsigned int pre, key *post, const Arg *arg, void *v)
{
    vcursctx_mode_switch(ctx, mode_normal);
}
void
cmd_prompt_curs_left(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_left(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_right(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_right(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_start(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_start(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_end(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_end(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_bs(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_bs(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_dc(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_curs_dc(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_curs_dl(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_freebuf(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_ins(unsigned int pre, key *post, const Arg *arg, void *v)
{
    if (!post[0].utfk)
        return;
    prompt_ins(cmd_prompt, post[0].ku.r);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_complete(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt_complete(cmd_prompt);
    prompt_print(cmd_prompt);
}
void
cmd_prompt_ins_digraph(unsigned int pre, key *post, const Arg *arg, void *v)
{
    if (!post[0].utfk || !post[1].utfk)
        return;
    if (post[0].ku.r < 0 || post[0].ku.r > 0x7F)
        return;
    if (post[1].ku.r < 0 || post[1].ku.r > 0x7F)
        return;
    Rune r = digraphtorune(post[0].ku.r, post[1].ku.r);
    if (r == Runeerror)
        return;
    prompt_ins(cmd_prompt, r);
    prompt_print(cmd_prompt);
}
