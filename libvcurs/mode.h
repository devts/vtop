typedef struct mode mode;
typedef struct vcursctx vcursctx;

struct mode {
    char **prompt_hist;
    size_t s_prompt_hist;
    size_t m_prompt_hist;
    size_t idx_hist_cur;
    void *mv;
    modebinds *mb;
    void (*start_mode)(vcursctx *, mode *);
    void (*end_mode)(vcursctx *, mode *);
};

mode *mode_new(char **prompt_hist, size_t s_prompt_hist, size_t m_prompt_hist,
    void *mv, modebinds *mb,
    void (*start_mode)(vcursctx *, mode *), void (*end_mode)(vcursctx *, mode *));
void mode_delete(mode *m);

struct vcursctx {
    keyhandler *kh;
    void *v;
    mode *cur;
    mode **modes;
    size_t num_modes;
};

vcursctx *vcursctx_new(keyhandler *kh, void *v, size_t num_modes);
void vcursctx_delete(vcursctx *ctx);
void vcursctx_mode_switch(vcursctx *ctx, mode *m);
void vcursctx_handle_curses_key(vcursctx *ctx, int ck);
