#include "../all.h"

mode *mode_new(char **prompt_hist, size_t s_prompt_hist, size_t m_prompt_hist,
    void *mv, modebinds *mb,
    void (*start_mode)(vcursctx *, mode *), void (*end_mode)(vcursctx *, mode *)) {
    mode *m = emalloc(sizeof(mode));
    m->prompt_hist = prompt_hist;
    m->s_prompt_hist = s_prompt_hist;
    m->m_prompt_hist = m_prompt_hist;
    m->idx_hist_cur = s_prompt_hist;
    m->mv = mv;
    m->mb = mb;
    m->start_mode = start_mode;
    m->end_mode = end_mode;
    return m;
}

void mode_delete(mode *m) {
    for (size_t i = 0; i < m->s_prompt_hist; i++)
        free(m->prompt_hist[i]);
    free(m->prompt_hist);
    free(m);
}

vcursctx *
vcursctx_new(keyhandler *kh, void *v, size_t num_modes) {
    vcursctx *ctx = emalloc(sizeof(vcursctx));
    ctx->kh = kh;
    ctx->v = v;
    ctx->cur = 0;
    ctx->modes = ecalloc(num_modes, sizeof(mode));
    ctx->num_modes = num_modes;
    return ctx;
}

void
vcursctx_delete(vcursctx *ctx) {
    free(ctx->modes);
    free(ctx);
}

void
vcursctx_mode_switch(vcursctx *ctx, mode *m) {
    if (ctx->cur)
        ctx->cur->end_mode(ctx, ctx->cur);
    keyhandler_switch_mode(ctx->kh, m ? m->mb : 0);
    ctx->cur = m;
    if (m)
        m->start_mode(ctx, m);
}

void
vcursctx_handle_curses_key(vcursctx *ctx, int ck) {
    keyhandler_handle_curses_key(ctx->kh, ck, ctx->v);
}
