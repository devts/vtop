#include "../all.h"

prompt *prompt_new(WINDOW *w, int attr, char prefix,
        void (*fstrchg)(char *, size_t, void *), void* argf,
        void (*complete)(char *, size_t, size_t, void *), void *argc)
{
    prompt *p = ecalloc(1, sizeof(prompt));
    p->visible = 0;
    p->w = w;
    p->attr = attr;
    p->prefix = prefix;
    p->fstrchg = fstrchg;
    p->argf = argf;
    p->complete = complete;
    p->argc = argc;
    return p;
}

void
prompt_delete(prompt *p) {
    free(p->buf);
    free(p);
}

void
prompt_print(prompt *p)
{
    if (!p->visible)
        return;
    wattrset(p->w, p->attr);
    werase(p->w);
    wmove(p->w, 0, 0);
    if (p->prefix)
        waddnstr(p->w, &p->prefix, 1);
    int crow, ccol = 1;
    if (p->buf) {
        waddnstr(p->w, p->buf + p->scroll, p->curs - p->scroll);
        getyx(p->w, crow, ccol);
        waddnstr(p->w, p->buf + p->curs, p->buf_num - p->curs);
    }
    int row, col;
    getbegyx(p->w, row, col);
    wmove(stdscr, row, col + ccol);
    wrefresh(p->w);
}

void
prompt_setstr(prompt *p, char *s) {
    if (!s) {
        prompt_freebuf(p);
        return;
    }
    free(p->buf);
    p->buf = estrdup(s);
    p->buf_msize = p->buf_num = strlen(s);
    p->curs = p->scroll = 0;
}

void
prompt_freebuf(prompt *p) {
    free(p->buf);
    p->buf = 0;
    p->buf_msize = p->buf_num = p->curs = p->scroll = 0;
}

void
prompt_fix_scroll(prompt *p) {
    if (p->curs <= p->scroll) {
        p->scroll = p->curs;
        return;
    }
    int row, col;
    getmaxyx(p->w, row, col);
    size_t wlen = MAX(col - 1, 2);
    /* Req. 0 <= "wcwidth(curs - scroll)" <= wlen - 1 */
    p->buf[p->buf_num] = ' '; /*make cursor on end count as wcwidth 1*/
    size_t wcw = wcwidth_range(p->buf, p->buf_num + 1, p->scroll, p->curs);
    if (wcw > wlen)
        wcwidth_seek_limited(p->buf, p->buf_num + 1, p->curs, &p->scroll, &wcw, -wlen);
    p->buf[p->buf_num] = 0;
}

void
prompt_fix_curs(prompt *p) {
    /* Req. 0 < curs <= buf_num */
    if (p->curs > p->buf_num)
        p->curs = p->buf_num;
}

void
prompt_fix_nullterm(prompt *p) {
    if (p->buf) {
        list_minsize((void **)&p->buf, &p->buf_num, &p->buf_msize, 1, p->buf_num + 1);
        p->buf[p->buf_num] = 0;
    } else {
        p->scroll = p->curs = 0;
    }
}

void
prompt_fix(prompt *p) {
    prompt_fix_nullterm(p);
    prompt_fix_curs(p);
    prompt_fix_scroll(p);
}

void
prompt_delete_line(prompt *p) {
    p->buf_num = p->curs = p->scroll = 0;
    if (p->fstrchg)
        p->fstrchg(p->buf, p->buf_num, p->argf);
}

void
prompt_curs_left(prompt *p) {
    if (p->curs)
        utfprev(p->buf, p->buf_num + 1, p->curs, &p->curs);
    prompt_fix(p);
}

void
prompt_curs_right(prompt *p) {
    if (p->curs < p->buf_num)
        utfnext(p->buf, p->buf_num + 1, p->curs, &p->curs);
    prompt_fix(p);
}

void
prompt_curs_start(prompt *p) {
    p->curs = 0;
    prompt_fix(p);
}

void
prompt_curs_end(prompt *p) {
    p->curs = p->buf_num;
    prompt_fix(p);
}

void
prompt_curs_bs(prompt *p) {
    if (!p->buf_num || !p->curs)
        return;
    size_t prev = p->curs;
    utfprev(p->buf, p->buf_num + 1, p->curs, &prev);
    list_remove_idx_range((void **)&p->buf, &p->buf_num, &p->buf_msize, 1, prev, p->curs - prev);
    p->curs = prev;
    prompt_fix(p);
    if (p->fstrchg)
        p->fstrchg(p->buf, p->buf_num, p->argf);
}

void
prompt_curs_dc(prompt *p) {
    if (!p->buf_num || p->curs == p->buf_num)
        return;
    size_t next = p->curs;
    utfnext(p->buf, p->buf_num + 1, p->curs, &next);
    list_remove_idx_range((void **)&p->buf, &p->buf_num, &p->buf_msize, 1, p->curs, next - p->curs);
    prompt_fix(p);
    if (p->fstrchg)
        p->fstrchg(p->buf, p->buf_num, p->argf);
}

void
prompt_ins(prompt *p, Rune r) {
    char uch[UTFmax];
    size_t num_uch = runetocharn(uch, sizeof(uch), &r);
    if (!num_uch)
        return;
    list_insert_range_at((void **)&p->buf, &p->buf_num, &p->buf_msize, 1, p->curs, num_uch, uch);
    p->curs += num_uch;
    prompt_fix(p);
    if (p->fstrchg)
        p->fstrchg(p->buf, p->buf_num, p->argf);
}

void
prompt_complete(prompt *p) {
    if (p->complete)
        p->complete(p->buf, p->buf_num, p->buf_msize, p->argc);
}
