typedef struct selitem selitem;
struct selitem
{
    size_t n;
    pid_t pid;
};

typedef struct procview procview;
struct procview
{
    procinfo **pl;
    procinfo *apl;
    size_t pl_size;
    struct timespec plt;

    procinfo **opl;
    procinfo *oapl;
    size_t opl_size;
    struct timespec oplt;

    selitem off;
    selitem cur;

    selitem *selection;
    size_t selection_size;

};

procview *dv_new(void);
void dv_del(procview *dv);
int dv_update(procview *dv);
void dv_free_dirlist(procview *dv);

/*1 = error. On Error values are reset to nsel=noff=0
 * and sel and off are set to consitent values
 * if dirlist_size > 0 and to NULL otherwise*/
int dv_update_cur(procview *dv, pid_t cur);
int dv_update_off(procview *dv, pid_t off);
int dv_update_ncur(procview *dv, size_t ncur);
int dv_update_noff(procview *dv, size_t noff);
int dv_update_selitem_pid(procview *dv, selitem *sel, pid_t pid, int resel);
int dv_update_selitem_n(procview *dv, selitem *sel, size_t nsel);
void dv_sort(procview *dv);

int dv_select_item(procview *dv, size_t n);
int dv_unselect_item(procview *dv, size_t n);
int dv_toggleselect_item(procview *dv, size_t n);
int dv_empty_selection(procview *dv);
int dv_regsel(procview *dv, char *reg);
int dv_iterate_as(procview *dv, int (*func)(pid_t pid, void *v), void *v);

int dv_search_next(procview *dv, char *reg, size_t *nr);
int dv_search_prev(procview *dv, char *reg, size_t *pr);
