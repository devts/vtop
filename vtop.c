#include "all.h"

tab *t = 0;
prompt *cmd_prompt = 0;

void
layout_win(void)
{
    int mrow,mcol;
    getmaxyx(stdscr,mrow,mcol);
    if (cmd_prompt)
        delwin(cmd_prompt->w);
    else
        cmd_prompt = prompt_new(0, scheme.DEFAULT_ATTR, ' ', 0, 0, 0, 0);
    cmd_prompt->w = subwin(stdscr, 1, mcol, mrow - 1, 0);
}
void
print_all(void)
{
    if (t)
        tab_print(t, stdscr);
    if (cmd_prompt)
        prompt_print(cmd_prompt);
}
void
cleanup_exit(int i)
{
    keyhandler_delete(ctx->kh);
    ctx->kh = 0;
    ctx->cur = 0;
    ctx->v = 0;
    vcursctx_delete(ctx);
    modes_free();
    if (t)
        tab_del(t);
    if(cmd_prompt)
        prompt_delete(cmd_prompt);
    endwin();
    exit(i);
}

#ifndef KEY_RESIZE
int resize = 0;
void
hresize(int i)
{
    resize = 1;
}
#endif
int ragetch(int err)
{
    int i;
    do
    {
        i = getch();
#ifdef KEY_RESIZE
        if (i==KEY_RESIZE) {
            layout_win();
            print_all();
        }
    } while(i == KEY_RESIZE || (!err && i == ERR));
#else
        if(resize)
        {
            endwin();
            layout_win();
            print_all();
            refresh();
            resize = 0;
        }
    } while(!err && i == ERR);
#endif
    return i;
}
char** errors = NULL;
size_t s_errors = 0;
int
main(int argc, char *argv[])
{
    setlocale(LC_ALL, "");
    if (argc > 1)
        enprintf(2, "Too many arguments\n");
    signal(SIGCHLD, SIG_IGN);
#ifndef KEY_RESIZE
    signal(SIGWINCH, hresize);
#endif
    initscr();
    cbreak();
    noecho();
    halfdelay(10);
    refresh();
    curs_set(0);
    keypad(stdscr, TRUE);
    if (has_colors() == FALSE) {
        memset(&scheme, 0, sizeof(scheme));
    } else {
        start_color();
        initcolorscheme();
    }

    t = tab_new();
    if (!t)
        cleanup_exit(1);
    layout_win();

    ctx = vcursctx_new(keyhandler_new(0), t, 3);
    modes_init();
    ctx->modes[0] = mode_normal;
    ctx->modes[1] = mode_cmd;
    ctx->modes[2] = mode_search;
    vcursctx_mode_switch(ctx, mode_normal);

    int ch = 0;
    while (1) {
        if ((ch = ragetch(1)) != ERR) {
            ctx->v = t;
            vcursctx_handle_curses_key(ctx, ch);
        } else {
            dv_update(t->dv);
            print_all();
        }
        if (errors) {
            cmd_prompt->visible = 1;
            cmd_prompt->attr = scheme.ERROR_ATTR;
            cmd_prompt->prefix = 0;
            for (size_t i = 0; i < s_errors; i++) {
                prompt_setstr(cmd_prompt, errors[i]);
                prompt_print(cmd_prompt);
                if (ragetch(0) == 27)
                    break;
            }
            prompt_freebuf(cmd_prompt);
            for (size_t i = 0; i < s_errors; i++)
                free(errors[i]);
            s_errors = 0;
            free(errors);
            errors = 0;
            cmd_prompt->prefix = ' ';
            cmd_prompt->attr = scheme.DEFAULT_ATTR;
            cmd_prompt->visible = 0;
            print_all();
        }
    }
    return 0;
}
