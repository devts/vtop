#include "all.h"

procview *dv_new(void)
{
    procview *dv;
    dv = ecalloc(1, sizeof(procview));
    dv->selection = 0;
    if (dv_update(dv))
        return 0;
    return dv;
}
void dv_del(procview *dv)
{
    dv_empty_selection(dv);
    free_pl(dv->apl, dv->pl_size);
    free_pl(dv->oapl, dv->opl_size);
    if (dv->pl)
        free(dv->pl);
    if (dv->opl)
        free(dv->opl);
    free(dv);
}

int dv_update(procview *dv)
{
    procinfo *apl;
    procinfo **pl;
    size_t pl_size;
    struct timespec plt;
    size_t i;
    if (clock_gettime(CLOCK_MONOTONIC, &plt) < 0)
        return 1;
    if (get_pl(&apl, &pl_size, HIDE_KTHREADS | HIDE_UTHREADS))
        return 1;
    pl = ecalloc(pl_size, sizeof(procinfo *));
    for (i = 0; i < pl_size; i++)
        pl[i] = apl + i;
    free_pl(dv->oapl, dv->opl_size);
    if (dv->opl)
        free(dv->opl);
    dv->opl = dv->pl;
    dv->oapl = dv->apl;
    dv->opl_size = dv->pl_size;
    dv->oplt = dv->plt;
    dv->pl = pl;
    dv->apl = apl;
    dv->pl_size = pl_size;
    dv->plt = plt;
    dv_sort(dv);
    return 0;
}

static int
selcmp(const void *a, const void *b)
{
    return NUMCMP(((selitem*)a)->n, ((selitem*)b)->n);
}
static int dv_selection_remove(procview *dv, size_t i);
static int
sortcmp(procinfo *pa, procinfo *pb)
{
    switch (sort) {
    default:
    case COL_PID:
        return NUMCMP(pa->pid, pb->pid);
    case COL_RSS:
        return NUMCMP(pa->rss, pb->rss);
    }
}
static int
proccmp(const void *a, const void *b)
{
    int xchg = 1;
    size_t ldiff;
    procinfo *pa = *(procinfo **)a;
    procinfo *pb = *(procinfo **)b;
    procinfo *pi, *pj;
    if (!tree)
        return sortcmp(pa, pb);
    if (pa->level > pb->level) {
        pi = pa;
        pa = pb;
        pb = pi;
        xchg = -1;
    }
    ldiff = pb->level - pa->level;
    pi = pa;
    pj = pb;
    while (ldiff) {
        pj = pj->ppi;
        ldiff--;
    }
    if (pi == pj)
        return -1 * xchg;
    while (pi->ppi != pj->ppi) {
        pi = pi->ppi;
        pj = pj->ppi;
    }
    return xchg * sortcmp(pi, pj);
}
void
dv_sort(procview *dv)
{
    qsort(dv->pl, dv->pl_size, sizeof(procinfo *), proccmp);
    dv_update_cur(dv, dv->cur.pid);
    dv_update_off(dv, dv->off.pid);
    if(!dv->selection)
        return;
    size_t i;
    for(i = 0; i < dv->selection_size; i++) {
        dv_update_selitem_pid(dv, dv->selection + i, dv->selection[i].pid, 0);
        if(!dv->selection[i].pid)
            dv_selection_remove(dv, i--);
    }
    if(!dv->selection)
        return;
    qsort(dv->selection, dv->selection_size, sizeof(selitem), selcmp);
    dv->selection = reallocarray(dv->selection, dv->pl_size, sizeof(selitem));
}

int dv_update_cur(procview *dv, pid_t cur)
{
    return dv_update_selitem_pid(dv, &dv->cur, cur, 1);
}
int dv_update_off(procview *dv, pid_t off)
{
    return dv_update_selitem_pid(dv, &dv->off, off, 1);
}
int dv_update_ncur(procview *dv, size_t ncur)
{
    return dv_update_selitem_n(dv, &dv->cur, ncur);
}
int dv_update_noff(procview *dv, size_t noff)
{
    return dv_update_selitem_n(dv, &dv->off, noff);
}
int 
dv_update_selitem_pid(procview *dv, selitem *sel, pid_t pid, int resel)
{
    if (pid) {
        for (size_t i = 0; i < dv->pl_size ; i++) {
            if (dv->pl[i]->pid == pid) {
                sel->n = i;
                return 0;
            }
        }
    }
    if(resel && dv->pl_size > 0 ) {
        sel->n = sel->n < dv->pl_size ? sel->n : dv->pl_size - 1;
        sel->pid = dv->pl[sel->n]->pid;
    } else {
        sel->n = 0;
        sel->pid = 0;
    }
    return 1;
}
int
dv_update_selitem_n(procview *dv, selitem *sel, size_t nsel)
{
    if (nsel < dv->pl_size) {
        sel->n = nsel;
        sel->pid = dv->pl[nsel]->pid;
        return 0;
    }
    return 1;
}

static regex_t preg;
static int h_preg=0;
/*passing NULL for reg means ue previous regex
 * and get the next match after nsel*/
/*returns 0 on found result, 1 on error, 2 on no next result found*/
int 
dv_search_next(procview *dv, char *reg, size_t *nr)
{
    if(!h_preg && !reg)
        return 1;
    if(reg)
    {
       if(h_preg)
            regfree(&preg); 
       if(regcomp(&preg, reg, REG_EXTENDED))
            return 1;
        h_preg = 1;
    }
    size_t ncur = dv->cur.n;
    size_t dirl = dv->pl_size;
    procinfo** dirs = dv->pl;
    for(size_t i = ncur + (reg ? 0 : 1 ); i < dirl; i++)
    {
        if(!regexec(&preg, dirs[i]->cmdline, 0, NULL, 0))
        {
            *nr = i;
            return 0;
        }
    }
    for(size_t i = 0; i < ncur; i++)
    {
        if(!regexec(&preg, dirs[i]->cmdline, 0, NULL, 0))
        {
            *nr = i;
            return 0;
        }
    }
    return 2;
}
int 
dv_search_prev(procview *dv, char *reg, size_t *pr)
{
    if(!h_preg && !reg)
        return 1;
    if(reg)
    {
       if(h_preg)
            regfree(&preg); 
       if(regcomp(&preg, reg, REG_EXTENDED))
            return 1;
        h_preg = 1;
    }
    size_t ncur = dv->cur.n;
    size_t dirl = dv->pl_size;
    procinfo** dirs = dv->pl;
    if(dirl == 0)
        return 2;
    if(ncur > 0)
        for(size_t i = ncur - (reg ? 0 : 1 ); 1 ; i--)
        {
            if(!regexec(&preg, dirs[i]->cmdline, 0, NULL, 0))
            {
                *pr = i;
                return 0;
            }
            if(i == 0)
                break;
        }
    for(size_t i = dirl - 1; i > ncur; i--)
    {
        if(!regexec(&preg, dirs[i]->cmdline, 0, NULL, 0))
        {
            *pr = i;
            return 0;
        }
    }
    return 2;
}

static int
dv_selection_insert(procview *dv, size_t n, size_t i)
{
    if (!dv->selection) {
        dv->selection = calloc(dv->pl_size, sizeof(selitem));
    }
    if (i > dv->selection_size)
        return 1;
    if (i != dv->selection_size)
        memmove(dv->selection + i + 1, dv->selection + i, (dv->selection_size - i)*sizeof(selitem));
    dv->selection_size++;
    dv->selection[i].n = n;
    dv->selection[i].pid = dv->pl[n]->pid;
    return 0;
}

static int
dv_selection_remove(procview *dv, size_t i)
{
    if (i >= dv->selection_size)
        return 1;
    dv->selection[i].n = 0;
    dv->selection[i].pid = 0;
    memmove(dv->selection + i, dv->selection + i + 1, (dv->selection_size - i - 1)*sizeof(selitem));
    dv->selection_size--;
    if (!dv->selection_size) {
        free(dv->selection);
        dv->selection = 0;
    }
    return 0;
}

static int
dv_manage_selection(procview *dv, size_t n, int insert, int toggle, int delete)
{
    if(n >= dv->pl_size)
        return 1;
    size_t i;
    for (i = 0; i < dv->selection_size && dv->selection[i].n < n; i++) { }
    if (i == dv->selection_size) {
        if (delete)
            return 0;
        return dv_selection_insert(dv, n, i);
    }
    if (dv->selection[i].n == n) {
        if (insert)
            return 0;
        return dv_selection_remove(dv, i);
    }
    if (delete)
        return 0;
    return dv_selection_insert(dv, n, i);
}
int
dv_select_item(procview *dv, size_t n)
{
    return dv_manage_selection(dv, n, 1, 0, 0);
}
int
dv_toggleselect_item(procview *dv, size_t n)
{
    return dv_manage_selection(dv, n, 0, 1, 0);
}
int
dv_unselect_item(procview *dv, size_t n)
{
    return dv_manage_selection(dv, n, 0, 0, 1);
}
int
dv_empty_selection(procview *dv)
{
    if (dv->selection) {
        free(dv->selection);
        dv->selection = 0;
        dv->selection_size = 0;
    }
    return 0;
}
int
dv_regsel(procview *dv, char *reg)
{
    regex_t creg;
    if (regcomp(&creg, reg, REG_EXTENDED))
        return 1;
    size_t dirl = dv->pl_size;
    procinfo** dirs = dv->pl;
    size_t i;
    for (i = 0; i < dirl; i++)
        if(!regexec(&creg, dirs[i]->cmdline, 0, NULL, 0))
            dv_select_item(dv, i);
    regfree(&creg);
    return 0;
}
int
dv_iterate_as(procview *dv, int (*func)(pid_t pid, void *v), void *v)
{
    int res = 0;
    size_t i;
    if(dv->selection)
        for(i = 0; i < dv->selection_size; i++)
            res |= func(dv->selection[i].pid, v);
    else
        res |= func(dv->cur.pid, v);
    return res;
}
