#include "all.h"

static int
strsize(off_t n, char* buf, size_t s_buf)
{
    const char postfixes[] = "BKMGTPE";
    double size;
    int i;

    for (size = n, i = 0; size >= 1024 && i < strlen(postfixes); i++)
        size /= 1024;

    if (!i)
        return snprintf(buf, s_buf, "%4juB", (uintmax_t)n);
    else
        return snprintf(buf, s_buf, "%4.0f%c", size, postfixes[i]);
}
#define GETMAXFIELD(a, s, p, v) for(size_t _i = 0; _i < s; _i++) { v = !_i || a[_i]->p > v ? a[_i]->p : v; }
static int getlen(long long l)
{
    int i = 0;
    if (!l)
        return 1;
    while (l) {
        l /= 10;
        i++;
    }
    return i;
}
int
tab_print(tab *t, WINDOW *w)
{
    if(!t || !t->dv)
        return 1;
    static char *buf = 0;
    static size_t s_buf = 0;
    int mrow, mcol;
    getmaxyx(w, mrow, mcol);
    uid_t myeuid = geteuid();
    t->mrow = mrow;
    t->mcol = mcol;
    tab_bring_cur_in_view(t);
    if (MAX(4096, mcol + 1) > s_buf)
        buf = erealloc(buf, MAX(4096, mcol + 1));
    werase(w);

    if (!mrow)
        goto tab_print_cleanup;

    procinfo **pl = t->dv->pl;
    size_t pl_size = t->dv->pl_size;
    procinfo *pi;
    if(!pl)
        goto tab_print_cleanup;
    size_t noff = t->dv->off.n;
    size_t ncur = t->dv->cur.n;
    size_t s_left;
    int attr;
    char vind;
    size_t seli = 0;
    while (seli < t->dv->selection_size && t->dv->selection[seli].n < noff)
        seli++;

    char ttystr[TTY_NAME_MAX];
    int tty_maj, tty_min;
    unsigned long sutime;
    time_t start;
    struct sysinfo info;
    if (sysinfo(&info) < 0)
        ;/*eprintf("sysinfo:");*/
    struct passwd *pw;
    long sc_clk_tck = sysconf(_SC_CLK_TCK);
    long page_size = sysconf(_SC_PAGESIZE);

    pid_t pidmax = 0;
    GETMAXFIELD(pl, pl_size, pid, pidmax);
    int pid_len = getlen(pidmax);
    pid_len = pid_len < 4 ? 4 : pid_len;
    pid_t ppidmax = 0;
    GETMAXFIELD(pl, pl_size, ppid, ppidmax);
    int ppid_len = getlen(ppidmax);
    ppid_len = ppid_len < 4 ? 4 : ppid_len;

    attr = scheme.HEADER_ATTR;
    wattrset(w, attr);
    memset(buf, 0, mcol + 1);
    s_left = 1;
    for (size_t j = 0; j < s_cols; j++) {
        switch (cols[j]) {
        default:
            break;
        case COL_PID:
            snprintf(buf + s_left, mcol - s_left, "PID");
            s_left += pid_len + 1;
            break;
        case COL_UID:
            snprintf(buf + s_left, mcol - s_left, "UID");
            s_left += 9;
            break;
        case COL_PPID:
            snprintf(buf + s_left, mcol - s_left, "PPID");
            s_left += ppid_len + 1;
            break;
        case COL_TTY_NAME:
            snprintf(buf + s_left, mcol - s_left, "TTY");
            s_left += 6;
            break;
        case COL_VSIZE:
            snprintf(buf + s_left, mcol - s_left, "VSIZE");
            s_left += 6;
            break;
        case COL_RSS:
            snprintf(buf + s_left, mcol - s_left, "  RSS");
            s_left += 6;
            break;
        case COL_STATE:
            snprintf(buf + s_left, mcol - s_left, "S");
            s_left += 2;
            break;
        case COL_NICE:
            snprintf(buf + s_left, mcol - s_left, " NI");
            s_left += 4;
            break;
        case COL_NUM_THREADS:
            snprintf(buf + s_left, mcol - s_left, "THR");
            s_left += 4;
            break;
        case COL_STIME:
            snprintf(buf + s_left, mcol - s_left, "STIME");
            s_left += 6;
            break;
        case COL_SUTIME_FINE:
            snprintf(buf + s_left, mcol - s_left, "TIME");
            s_left += 12;
            break;
        case COL_CPU_PERCENT:
            snprintf(buf + s_left, mcol - s_left, "CPU%%");
            s_left += 5;
            break;
        }
    }
    snprintf(buf + s_left, mcol - s_left, "CMD");
    for (size_t j = 0; j < mcol; j++)
        if (!buf[j])
            buf[j] = ' ';
    mvwaddnstr(w, 0, 0, buf, mcol);

    for (size_t i = noff; i < pl_size && i < mrow - 1 + noff; i++) {
        pi = *(pl + i);
        attr = scheme.DEFAULT_ATTR;
        vind = ' ';
        if (myeuid == t->dv->pl[i]->euid)
            attr = scheme.PROC_SELF_ATTR;
        else
            attr = scheme.PROC_OTHR_ATTR;
        if (seli < t->dv->selection_size && t->dv->selection[seli].n == i) {
            attr = scheme.SELECTED_ATTR;
            vind = '*';
            seli++;
        }
        if (i == ncur) {
            attr |= scheme.CUR_OR_ATTR;
            vind = '>';
        }
        wattrset(w, attr);
        memset(buf, 0, mcol + 1);
        buf[0] = vind;
        s_left = 1;
        for (size_t j = 0; j < s_cols; j++) {
            switch (cols[j]) {
            default:
                break;
            case COL_PID:
                snprintf(buf + s_left, mcol - s_left, "%d", pi->pid);
                s_left += pid_len + 1;
                break;
            case COL_UID:
                pw = getpwuid(pi->uid);
                if (pw)
                    snprintf(buf + s_left, mcol - s_left, "%-8s", pw->pw_name);
                s_left += 9;
                break;
            case COL_PPID:
                snprintf(buf + s_left, mcol - s_left, "%d", pi->ppid);
                s_left += ppid_len + 1;
                break;
            case COL_TTY_NAME:
                devtotty(pi->tty_nr, &tty_maj, &tty_min);
                ttytostr(tty_maj, tty_min, ttystr, sizeof(ttystr));
                snprintf(buf + s_left, mcol - s_left, "%-5s", ttystr);
                s_left += 6;
                break;
            case COL_VSIZE:
                strsize(pi->vsize, buf + s_left, mcol - s_left);
                s_left += 6;
                break;
            case COL_RSS:
                strsize(pi->rss * page_size, buf + s_left, mcol - s_left);
                s_left += 6;
                break;
            case COL_STATE:
                buf[s_left] = pi->state;
                s_left += 2;
                break;
            case COL_NICE:
                snprintf(buf + s_left, mcol - s_left, "%3ld", pi->nice);
                s_left += 4;
                break;
            case COL_NUM_THREADS:
                snprintf(buf + s_left, mcol - s_left, "%3ld", pi->num_threads);
                s_left += 4;
                break;
            case COL_STIME:
                start = time(NULL) - info.uptime;
                start += (pi->starttime / sc_clk_tck);
                strftime(buf + s_left, mcol - s_left, "%H:%M", localtime(&start));
                s_left += 6;
                break;
            case COL_SUTIME_FINE:
                sutime = (pi->stime + pi->utime) * 100 / sc_clk_tck;
                snprintf(buf + s_left, mcol - s_left, "%02lu:%02lu:%02lu.%02lu",
                    sutime / 360000, (sutime % 360000) / 6000, (sutime % 6000) / 100, sutime % 100);
                s_left += 12;
                break;
            case COL_CPU_PERCENT:
                sutime = (pi->stime + pi->utime) * 100 / sc_clk_tck;
                long osutime;
                int r = 0;
                for (size_t n = 0; n < t->dv->opl_size; n++) {
                    if (t->dv->opl[n]->pid == pi->pid) {
                        osutime = (t->dv->opl[n]->stime + t->dv->opl[n]->utime) * 100 / sc_clk_tck;
                        r = 1;
                        break;
                    }
                }
                if (!r)
                    osutime = sutime;
                long dt = (t->dv->plt.tv_sec - t->dv->oplt.tv_sec) * 100 +
                    (t->dv->plt.tv_nsec / 10000000 - t->dv->oplt.tv_sec / 10000000);
                if (!dt)
                    dt = 1;
                double cper = (double)(sutime - osutime) / (double)dt;
                snprintf(buf + s_left, mcol - s_left, "%4.1f", cper * 100);
                s_left += 5;
                break;
            }
        }
        snprintf(buf + s_left + (tree ? 2*pi->level : 0), mcol - s_left - (tree ? 2*pi->level : 0), "%s", pi->cmdline);
        for (size_t j = 0; j < mcol; j++)
            if (!buf[j] || buf[j] == '\n')
                buf[j] = ' ';
        mvwaddnstr(w, i - noff + 1, 0, buf, mcol);
    }

tab_print_cleanup:
    wrefresh(w);
    return 0;
}

void
tab_bring_cur_in_view(tab *t)
{
    size_t mrow = t->mrow;
    if (mrow < 2 || !t->dv->pl_size)
        return;
    mrow -= 1;
    size_t eso = MIN((mrow - 1) / 2, so);
    size_t ws = mrow - 2*eso;
    if (!ws)
        return;
    size_t off = t->dv->off.n + eso;
    if (t->dv->cur.n < off)
        off = t->dv->cur.n;
    if (t->dv->cur.n > off + ws - 1)
        off = t->dv->cur.n + 1 - ws;
    if (off + eso + ws >= t->dv->pl_size)
        off = t->dv->pl_size > ws + eso ? t->dv->pl_size - ws - eso : 0;
    dv_update_noff(t->dv, off > eso ? off - eso : 0);
}
int
tab_move_to(tab *t, size_t n)
{
    if(!t)
        return 1;
    if(dv_update_ncur(t->dv, n))
        return 1;
    return 0;
}
int
tab_move_rel(tab *t, int n)
{
    if(!t)
        return 1;
    return tab_move_to(t, t->dv->cur.n + n);
}
void
tab_move_top(tab *t)
{
    size_t mrow = t->mrow;
    if (mrow < 2 || !t->dv->pl_size)
        return;
    mrow -= 1;
    size_t eso = MIN((mrow - 1) / 2, so);
    if (!t->dv->off.n)
        tab_move_to(t, 0);
    else
        tab_move_to(t, t->dv->off.n + eso);
}
void
tab_move_middle(tab *t)
{
    size_t mrow = t->mrow;
    if (mrow < 2 || !t->dv->pl_size)
        return;
    mrow -= 1;
    tab_move_to(t, t->dv->off.n + MIN((t->dv->pl_size - 1)/2, (mrow - 1)/2));
}
void
tab_move_bottom(tab *t)
{
    size_t mrow = t->mrow;
    if (mrow < 2 || !t->dv->pl_size)
        return;
    mrow -= 1;
    size_t eso = MIN((mrow - 1) / 2, so);
    if (t->dv->pl_size <= t->dv->off.n + mrow)
        tab_move_to(t, t->dv->pl_size - 1);
    else
        tab_move_to(t, t->dv->off.n + mrow - 1 - eso);
}
void
tab_page_up(tab *t, size_t n, size_t m, size_t sl)
{
    size_t mrow = t->mrow;
    if (mrow < 2 || !t->dv->pl_size)
        return;
    mrow -= 1;
    if (m)
        sl = mrow * n / m;
    if (t->dv->cur.n > sl) {
        dv_update_ncur(t->dv, t->dv->cur.n - sl);
        dv_update_noff(t->dv, t->dv->off.n > sl ? t->dv->off.n - sl : 0);
    } else {
        dv_update_noff(t->dv, 0);
        dv_update_ncur(t->dv, 0);
    }
}
void
tab_page_down(tab *t, size_t n, size_t m, size_t sl)
{
    size_t mrow = t->mrow;
    if (mrow < 2 || !t->dv->pl_size)
        return;
    mrow -= 1;
    if (m)
        sl = mrow * n / m;
    if (t->dv->cur.n + sl < t->dv->pl_size) {
        dv_update_ncur(t->dv, t->dv->cur.n + sl);
        dv_update_noff(t->dv, t->dv->off.n + sl < t->dv->pl_size ?
                t->dv->off.n + sl : t->dv->pl_size - 1);
    } else {
        dv_update_noff(t->dv, t->dv->pl_size - 1);
        dv_update_ncur(t->dv, t->dv->pl_size - 1);
    }
    tab_bring_cur_in_view(t);
}
void
tab_cur_to_middle(tab *t)
{
    size_t mrow = t->mrow;
    if (mrow < 2 || !t->dv->pl_size)
        return;
    mrow -= 1;
    if (t->dv->cur.n > (mrow - 1) / 2)
        dv_update_noff(t->dv, t->dv->cur.n - (mrow - 1) / 2);
    else
        dv_update_noff(t->dv, 0);
}
void
tab_cur_to_top(tab *t)
{
    if (!t->dv->pl_size)
        return;
    dv_update_noff(t->dv, t->dv->pl_size - 1);
    tab_bring_cur_in_view(t);
}
void
tab_cur_to_bottom(tab *t)
{
    if (!t->dv->pl_size)
        return;
    dv_update_noff(t->dv, 0);
    tab_bring_cur_in_view(t);
}
int
tab_search_next(tab *t, char *reg)
{
    if(!t || !t->dv)
        return 1;
    size_t nr;
    int res = dv_search_next(t->dv, reg, &nr);
    if(!res)
        tab_move_to(t, nr);
    return res;
}
int
tab_search_prev(tab *t, char *reg)
{
    if(!t || !t->dv)
        return 1;
    size_t nr;
    int res = dv_search_prev(t->dv, reg, &nr);
    if(!res)
        tab_move_to(t, nr);
    return res;
}
int
tab_del(tab* t)
{
    if (!t)
        return 1;
    dv_del(t->dv);
    free(t);
    return 0;
}

tab *
tab_new(void)
{
    tab *t = ecalloc(1, sizeof(tab)); 
    if (!(t->dv = dv_new())) {
        tab_del(t);
        return 0;
    }
    return t;
}
