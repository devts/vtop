typedef struct colorscheme colorscheme;
struct colorscheme
{
    int DEFAULT_ATTR;
    int ERROR_ATTR;
    int HEADER_ATTR;
    int PROC_SELF_ATTR;
    int PROC_OTHR_ATTR;
    int CUR_OR_ATTR;
    int SELECTED_ATTR;
};
colorscheme scheme;
int getcolor(short fg, short bg);
