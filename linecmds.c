#include "all.h"

void
cmd_regsel(size_t argc, char** argv, char *cmd, void *v)
{
    tab *t = v;
    if(argc != 1)
        return;
    dv_regsel(t->dv, argv[0]);
}
void
cmd_clearsel(size_t argc, char** argv, char *cmd, void *v)
{
    tab *t = v;
    if(argc != 0)
        return;
    dv_empty_selection(t->dv);
}
