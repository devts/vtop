typedef struct {
    /*stat file*/
    int pid;
    char comm[PATH_MAX + 2]; /* + 2 for '(' and ')' */
    unsigned char state;
    int ppid;
    int pgrp;
    int sid;
    int tty_nr;
    int tpgid;
    unsigned flags;
    unsigned long minflt;
    unsigned long cminflt;
    unsigned long majflt;
    unsigned long cmajflt;
    unsigned long utime;
    unsigned long stime;
    long cutime;
    long cstime;
    long priority;
    long nice;
    long num_threads;
    long itrealvalue;
    unsigned long long starttime;
    unsigned long vsize;
    long rss;
    long rsslim;
    /*status file*/
    uid_t uid;
    uid_t euid;
    gid_t gid;
    gid_t egid;
    pid_t tgid;
    /*cmdline file*/
    char cmdline[PATH_MAX];
} procstat;

typedef struct procinfo procinfo;
struct procinfo {
    pid_t pid;
    pid_t ppid;
    pid_t pgrp;
    pid_t tgid;
    int sid;
    int tty_nr;
    char *cmdline;
    unsigned char state;
    unsigned long utime;
    unsigned long stime;
    long nice;
    long num_threads;
    unsigned long long starttime;
    unsigned long vsize;
    long rss;
    uid_t uid;
    uid_t euid;
    gid_t gid;
    gid_t egid;
    procinfo *ppi;
    size_t level;
};

int HIDE_KTHREADS;
int HIDE_UTHREADS;

int get_pl(procinfo **pl, size_t *pl_size, int opts);
void free_pl(procinfo *pl, size_t pl_size);

void devtotty(int, int *, int *);
int ttytostr(int, int, char *, size_t);
