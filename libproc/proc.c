#include "../all.h"

int HIDE_KTHREADS = 1;
int HIDE_UTHREADS = 2;

int
parsecmdline(pid_t pid, procstat *ps)
{
    int fd;
    char path[PATH_MAX];
    ssize_t n, i;
    char *buf = ps->cmdline;
    size_t siz = sizeof(ps->cmdline);

    snprintf(path, sizeof(path), "/proc/%d/cmdline", pid);
    if ((fd = open(path, O_RDONLY)) < 0)
        return -1;
    n = read(fd, buf, siz > 0 ? siz - 1 : 0);
    close(fd);
    if (n < 0 || !n)
        return -1;
    buf[n] = '\0';
    for (i = 0; i < n; i++)
        if (buf[i] == '\0')
            buf[i] = ' ';
    return 0;
}

int
parsestat(pid_t pid, procstat *ps)
{
    char path[PATH_MAX];
    char buf[PATH_MAX];
    int fd;
    ssize_t n;
    char *s, *t;

    snprintf(path, sizeof(path), "/proc/%d/stat", pid);
    if ((fd = open(path, O_RDONLY)) < 0)
        return -1;
    n = read(fd, buf, sizeof(buf) - 1);
    close(fd);
    if (n < 0 || !n)
        return -1;
    buf[n] = '\0';
    s = buf;
    ps->pid = strtol(s, &s, 10);
    s += 2;
    t = strrchr(s, ')');
    if (!t)
        return 1;
    memcpy(ps->comm, s, MIN(t - s, sizeof(ps->comm)));
    s = t + 2;
    ps->state = *s++;
    ps->ppid = strtol(++s, &s, 10);
    ps->pgrp = strtol(++s, &s, 10);
    ps->sid = strtol(++s, &s, 10);
    ps->tty_nr = strtol(++s, &s, 10);
    ps->tpgid = strtol(++s, &s, 10);
    ps->flags = strtoul(++s, &s, 10);
    ps->minflt = strtoul(++s, &s, 10);
    ps->cminflt = strtoul(++s, &s, 10);
    ps->majflt = strtoul(++s, &s, 10);
    ps->cmajflt = strtoul(++s, &s, 10);
    ps->utime = strtoul(++s, &s, 10);
    ps->stime = strtoul(++s, &s, 10);
    ps->cutime = strtol(++s, &s, 10);
    ps->cstime = strtol(++s, &s, 10);
    ps->priority = strtol(++s, &s, 10);
    ps->nice = strtol(++s, &s, 10);
    ps->num_threads = strtol(++s, &s, 10);
    ps->itrealvalue = strtol(++s, &s, 10);
    ps->starttime = strtoull(++s, &s, 10);
    ps->vsize = strtoul(++s, &s, 10);
    ps->rss = strtol(++s, &s, 10);
    ps->rsslim = strtol(++s, &s, 10);
    return 0;
}

int
parsestatus(pid_t pid, procstat *ps)
{
    char path[PATH_MAX];
    char buf[PATH_MAX + 1024];
    int fd;
    ssize_t n;
    char *s;

    snprintf(path, sizeof(path), "/proc/%d/status", pid);
    if ((fd = open(path, O_RDONLY)) < 0)
        return -1;
    n = read(fd, buf, sizeof(buf) - 1);
    close(fd);
    if (n < 0 || !n)
        return -1;
    buf[n] = '\0';
    s = buf;

    s = strstr(s, "Tgid:");
    if (!s)
        return -1;
    s += 5;
    ps->tgid = strtol(++s, &s, 10);

    s = strstr(s, "Uid:");
    if (!s)
        return -1;
    s += 4;
    ps->uid = strtoul(++s, &s, 10);
    ps->euid = strtoul(++s, &s, 10);

    s = strstr(s, "Gid:");
    if (!s)
        return -1;
    s += 4;
    ps->gid = strtoul(++s, &s, 10);
    ps->egid = strtoul(++s, &s, 10);
    return 0;
}

static int
xstrtol(long *l, const char *s, int base)
{
    char *end;

    errno = 0;
    *l = strtol(s, &end, base);
    if (*end != '\0')
        return 1;
    if (errno != 0)
        return 1;
    return 0;
}

void
ps_to_pi(procstat *ps, procinfo *pi)
{
    pi->pid = ps->pid;
    pi->ppid = ps->ppid;
    pi->pgrp = ps->pgrp;
    pi->tgid = ps->tgid;
    pi->sid = ps->sid;
    pi->tty_nr = ps->tty_nr;
    if (*ps->cmdline)
        pi->cmdline = estrdup(ps->cmdline);
    else
        pi->cmdline = estrdup(ps->comm);
    pi->state = ps->state;
    pi->utime = ps->utime;
    pi->stime = ps->stime;
    pi->nice = ps->nice;
    pi->num_threads = ps->num_threads;
    pi->vsize = ps->vsize;
    pi->starttime = ps->starttime;
    pi->rss = ps->rss;
    pi->uid = ps->uid;
    pi->euid = ps->euid;
    pi->gid = ps->gid;
    pi->egid = ps->egid;
    pi->ppi = 0;
    pi->level = 0;
}

int
get_pl(procinfo **pl, size_t *pl_size, int opts)
{
    DIR *dp;
    struct dirent *d;
    size_t msize = 16;
    long pid;
    procstat ps;
    size_t i, j;
    procinfo *pi;
    if (!(dp = opendir("/proc")))
        return 1;
    *pl_size = 0;
    *pl = ereallocarray(NULL, msize, sizeof(procinfo));
    while ((d = readdir(dp))) {
        if (!strcmp(d->d_name, ".") || !strcmp(d->d_name, ".."))
            continue;
        if (xstrtol(&pid, d->d_name, 10))
            continue;
        if (parsestat(pid, &ps) < 0)
            continue;
        if (opts & HIDE_KTHREADS && !ps.pgrp && pid != 1)
            continue;
        if (parsestat(pid, &ps) < 0 || parsestatus(pid, &ps) < 0)
            continue;
        if (opts & HIDE_UTHREADS && ps.pgrp && pid != ps.tgid && pid != 1)
            continue;
        if (parsecmdline(pid, &ps) < 0)
            *ps.cmdline = 0;
        if (msize < ++(*pl_size))
            *pl = ereallocarray(*pl, (msize = msize << 1), sizeof(procinfo));
        ps_to_pi(&ps, *pl + *pl_size - 1);
    }
    closedir(dp);
    if (*pl_size)
        *pl = ereallocarray(*pl, *pl_size, sizeof(procinfo));
    else
        free(*pl), *pl = 0;
    for (i = 0; i < *pl_size; i++) {
        for (j = 0; j < *pl_size; j++) {
            if ((*pl)[i].ppid == (*pl)[j].pid) {
                (*pl)[i].ppi = *pl + j;
                break;
            }
        }
    }
    for (i = 0; i < *pl_size; i++) {
        pi = *pl + i;
        j = 0;
        while ((pi = pi->ppi))
            j++;
        (*pl)[i].level = j;
    }
    return 0;
}
void free_pl(procinfo *pl, size_t pl_size)
{
    if (!pl)
        return;
    size_t i;
    for (i = 0; i < pl_size; i++)
        free(pl[i].cmdline);
    free(pl);
}
